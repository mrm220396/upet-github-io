var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
//convert scss files to css
function sassCompiler(){
return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'])
  .pipe(sass())
  .pipe(gulp.dest('src/css'))
  .pipe(browserSync.stream());
};

//move js files to the src
function js() {
return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js','node_modules/popper.js/dist/popper.min.js', 'node_modules/jquery/dist/jquery.min.js' ])
  .pipe(gulp.dest('src/js'))
  .pipe(browserSync.stream());
}

gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'src/scss/*.scss'], gulp.parallel(sassCompiler));
gulp.watch('src/*.html').on('change', browserSync.reload);

//start server
gulp.task('default',gulp.series(js, sassCompiler));